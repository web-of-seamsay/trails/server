// TODO: Get leaflet to work as a module, allowing `import * as Leaflet from "./leaflet.js";`.
import "./leaflet.js";

const Leaflet = L;
const routeStyle = { color: "red" };

function map(mapDivId) {
    const map = Leaflet.map(mapDivId);

    Leaflet.tileLayer(
        "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
        { attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>' },
    ).addTo(map);

    return map;
}

export function trail(mapDivId, coordinateScriptId) {
    const mapObject = map(mapDivId);

    const coordinates = JSON.parse(document.getElementById(coordinateScriptId).text);
    const route = Leaflet.polyline(coordinates, routeStyle).addTo(mapObject);

    mapObject.fitBounds(route.getBounds());
}

function updateRoute(mapObject, coordinates, oldRoute) {
    mapObject.removeLayer(oldRoute);
    const newRoute = Leaflet.polyline(coordinates, routeStyle);
    mapObject.addLayer(newRoute);

    return newRoute;
}

function updateFormcoordinates(coordinatesListObject, coordinate) {
    function createSpan(type, index, value) {
        const spanObject = document.createElement("span");

        const id = `${type}-${index}`;

        const labelObject = document.createElement("label");
        labelObject.setAttribute("for", id);
        labelObject.innerText = type;

        spanObject.appendChild(labelObject);

        const inputObject = document.createElement("input");
        inputObject.id = id;
        inputObject.name = `coordinates[${index}].${type}`;
        inputObject.type = "number";
        if (type !== "order") {
            inputObject.step = "any";
        }
        inputObject.value = value;

        spanObject.appendChild(inputObject);

        return spanObject;
    }

    const maxOrder = Array.from(coordinatesListObject.querySelectorAll("input[id^='order-']"))
        .map((input) => input.valueAsNumber)
        .reduce((accumulator, element) => Math.max(accumulator, element), -1);
    const nextOrder = maxOrder + 1;

    const listItemObject = document.createElement("li");
    listItemObject.appendChild(createSpan("order", nextOrder, nextOrder));
    listItemObject.appendChild(createSpan("latitude", nextOrder, coordinate.lat));
    listItemObject.appendChild(createSpan("longitude", nextOrder, coordinate.lng));

    coordinatesListObject.appendChild(listItemObject);
}

export function editor(mapDivId, coordinatesListId) {
    const mapObject = map(mapDivId);
    mapObject.fitWorld();

    navigator.geolocation.getCurrentPosition(
        function (position) {
            const coordinates = Leaflet.latLng(
                position.coords.latitude,
                position.coords.longitude,
            );
            mapObject.setView(coordinates, 15);
        },
        console.warn,
    );

    const coordinates = [];
    let route = Leaflet.polyline(coordinates, routeStyle);
    mapObject.addLayer(route);
    mapObject.on(
        "click",
        function (event) {
            const coordinate = event.latlng;

            coordinates.push(coordinate);

            route = updateRoute(mapObject, coordinates, route);

            updateFormcoordinates(document.getElementById(coordinatesListId), coordinate);
        },
    )
}
