"""TODO: Would it be easier to scrape `https://en.wikipedia.org/wiki/Module:ISO_3166/data`?"""
import argparse
from logging.config import IDENTIFIER
import bs4
import copy
import json
import logging
import pyparsing
import requests


ROOT = "https://en.wikipedia.org/wiki/Module:ISO_3166/data/"


COMMENT = pyparsing.Literal("--") + pyparsing.Optional(pyparsing.rest_of_line)

KEYWORD_RETURN = pyparsing.Keyword("return")
KEYWORD_TRUE = pyparsing.Keyword("true").set_parse_action(pyparsing.replace_with(True))
KEYWORD_FALSE = pyparsing.Keyword("false").set_parse_action(
    pyparsing.replace_with(False)
)
IDENTIFIER = pyparsing.Word(pyparsing.identbodychars)

BRACKET_CURLY_LEFT = pyparsing.Suppress("{")
BRACKET_CURLY_RIGHT = pyparsing.Suppress("}")
BRACKET_SQUARE_LEFT = pyparsing.Suppress("[")
BRACKET_SQUARE_RIGHT = pyparsing.Suppress("]")

EQUALS = pyparsing.Suppress("=")

STRING = pyparsing.dbl_quoted_string().set_parse_action(pyparsing.remove_quotes)

VALUE = pyparsing.Forward()
KEY_VALUE = STRING

DICT_TABLE = pyparsing.Forward()
LITERAL_KEY = IDENTIFIER
VALUE_KEY = BRACKET_SQUARE_LEFT + KEY_VALUE + BRACKET_SQUARE_RIGHT
DICT_TABLE_MEMBER = pyparsing.Group(((VALUE_KEY ^ LITERAL_KEY) + EQUALS + VALUE))
DICT_TABLE_MEMBERS = (
    pyparsing.delimited_list(DICT_TABLE_MEMBER) + pyparsing.Optional(",").suppress()
)
DICT_TABLE << pyparsing.Dict(
    BRACKET_CURLY_LEFT + pyparsing.Optional(DICT_TABLE_MEMBERS) + BRACKET_CURLY_RIGHT,
    asdict=True,
)

LIST_TABLE = pyparsing.Forward()
LIST_TABLE_MEMBER = VALUE
LIST_TABLE_MEMBERS = (
    pyparsing.delimited_list(LIST_TABLE_MEMBER) + pyparsing.Optional(",").suppress()
)
LIST_TABLE << pyparsing.Group(
    BRACKET_CURLY_LEFT + pyparsing.Optional(LIST_TABLE_MEMBERS) + BRACKET_CURLY_RIGHT,
    aslist=True,
)

VALUE << (KEYWORD_TRUE ^ KEYWORD_FALSE ^ STRING ^ DICT_TABLE ^ LIST_TABLE)

PROGRAM = KEYWORD_RETURN + DICT_TABLE
PROGRAM.ignore(COMMENT)


def scrape(section):
    url = ROOT + section
    logging.info("Scraping page: %s", url)

    response = requests.get(url)
    html = bs4.BeautifulSoup(response.text, "html.parser")

    pres = html.find_all("pre")
    assert len(pres) == 1
    script = "\n".join(pres[0].stripped_strings)

    ast = PROGRAM.parse_string(script)
    assert ast[0] == "return"

    logging.info("Finished scraping page: %s", url)

    return ast[1]


def with_areas(old):
    new = {}
    for (alpha2, old_info) in old.items():
        if len(alpha2) == 6 and alpha2.startswith("GB-"):
            logging.info(f"Skipping GB area country: {alpha2}")
            continue
        elif len(alpha2) != 2:
            raise Exception(f"Invalid Alpha2: {alpha2}")

        areas = scrape(alpha2)

        info = copy.deepcopy(old_info)
        info["areas"] = areas

        new[alpha2] = info

    return new


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Scrape Wikipedia for ISO 3166 data.")
    parser.add_argument(
        "output", help="The file path at which the JSON data should be output."
    )
    parser.add_argument(
        "-l",
        "--log-level",
        default="info",
        help="The level at which to print log messages.",
    )

    arguments = parser.parse_args()

    logging.basicConfig(level=getattr(logging, arguments.log_level.upper()))

    data = with_areas(scrape("National"))

    with open(arguments.output, "w") as file:
        json.dump(data, file)
