#[derive(Debug, Eq, thiserror::Error, PartialEq, serde::Serialize)]
pub enum Alpha2CodeError {
    #[error("expected string of length 2, not {0}")]
    IncorrectLength(usize),
    #[error("`{0}` does not represent an uppercase ASCII character")]
    NotUppercase(#[serde(serialize_with = "super::serialize_byte_as_character")] u8),
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Alpha2Code([u8; 2]);

impl Alpha2Code {
    // TODO: Get rid of this once `try_from` can be const.
    pub(crate) const fn new(value: &str) -> Result<Self, Alpha2CodeError> {
        let length = value.len();
        let bytes = value.as_bytes();

        if length != 2 {
            Err(Alpha2CodeError::IncorrectLength(length))
        } else if !bytes[0].is_ascii_uppercase() {
            Err(Alpha2CodeError::NotUppercase(bytes[0]))
        } else if !bytes[1].is_ascii_uppercase() {
            Err(Alpha2CodeError::NotUppercase(bytes[1]))
        } else {
            Ok(Self([bytes[0], bytes[1]]))
        }
    }
}

impl TryFrom<&str> for Alpha2Code {
    type Error = Alpha2CodeError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        Alpha2Code::new(value)
    }
}

impl AsRef<str> for Alpha2Code {
    fn as_ref(&self) -> &str {
        std::str::from_utf8(&self.0).expect("Statically known to be valid UTF-8.")
    }
}

impl serde::Serialize for Alpha2Code {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(self.as_ref())
    }
}

struct Alpha2Visitor;

impl<'de> serde::de::Visitor<'de> for Alpha2Visitor {
    type Value = Alpha2Code;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        formatter.write_str("a string of two uppercase ASCII letters")
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        v.try_into().map_err(|error| match error {
            Alpha2CodeError::IncorrectLength(length) => {
                E::invalid_length(length, &"exactly two elements")
            }
            Alpha2CodeError::NotUppercase(byte) => E::invalid_value(
                serde::de::Unexpected::Char(byte.into()),
                &"only uppercase ASCII letters",
            ),
        })
    }
}

impl<'de> serde::Deserialize<'de> for Alpha2Code {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_str(Alpha2Visitor)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn correct() {
        assert_eq!(Alpha2Code::try_from("GB"), Ok(Alpha2Code([b'G', b'B'])));
    }

    #[test]
    fn incorrect_length() {
        assert_eq!(
            Alpha2Code::try_from("GBR"),
            Err(Alpha2CodeError::IncorrectLength(3))
        );
    }

    #[test]
    fn incorrect_capitalisation() {
        assert_eq!(
            Alpha2Code::try_from("Gb"),
            Err(Alpha2CodeError::NotUppercase(b'b'))
        );
    }

    #[test]
    fn incorrect_ascii_type() {
        assert_eq!(
            Alpha2Code::try_from("1B"),
            Err(Alpha2CodeError::NotUppercase(b'1'))
        );
    }

    #[test]
    fn round_trip() {
        assert_eq!(Alpha2Code::try_from("NZ").unwrap().as_ref(), "NZ");
    }

    #[test]
    fn serialization() {
        let code = Alpha2Code::try_from("DE").unwrap();
        let serialized = serde_json::to_string(&code).unwrap();
        assert_eq!(serialized, r#""DE""#);
    }

    #[test]
    fn deserialization() {
        let json = r#""AD""#;
        let code = serde_json::from_str::<Alpha2Code>(json).unwrap();
        assert_eq!(code, Alpha2Code::try_from("AD").unwrap());
    }
}
