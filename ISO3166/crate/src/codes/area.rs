#[derive(Clone, Debug, Eq, thiserror::Error, PartialEq, serde::Serialize)]
pub enum AreaCodeError {
    #[error("`{0}` does not represent an ASCII digit or uppercase letter")]
    NotUppercaseOrDigit(#[serde(serialize_with = "super::serialize_byte_as_character")] u8),
}

#[derive(Debug, Eq, PartialEq)]
#[repr(transparent)]
pub struct AreaCode([u8]);

impl AreaCode {
    // TODO: When const traits are stable, take `B: ?Sized + AsRef<[u8]>`.
    pub(crate) const fn new(value: &str) -> Result<&Self, AreaCodeError> {
        let bytes = value.as_bytes();

        let mut index = 0;
        while index < bytes.len() {
            let byte = bytes[index];
            if !byte.is_ascii_uppercase() && !byte.is_ascii_digit() {
                return Err(AreaCodeError::NotUppercaseOrDigit(byte));
            }

            index += 1;
        }

        // SAFETY: `repr(transparent)` guarantees they have the same layout.
        Ok(unsafe { std::mem::transmute(bytes) })
    }
}

impl std::fmt::Display for AreaCode {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        std::fmt::Display::fmt(self.as_ref(), f)
    }
}

impl<'a> TryFrom<&'a str> for &'a AreaCode {
    type Error = AreaCodeError;

    fn try_from(value: &'a str) -> Result<Self, Self::Error> {
        AreaCode::new(value)
    }
}

impl AsRef<str> for AreaCode {
    fn as_ref(&self) -> &str {
        std::str::from_utf8(&self.0).expect("Statically known to be valid UTF-8.")
    }
}

impl serde::Serialize for AreaCode {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(self.as_ref())
    }
}

#[derive(Default)]
struct AreaCodeVisitor<'a> {
    lifetime: std::marker::PhantomData<&'a ()>,
}

impl<'de: 'a, 'a> serde::de::Visitor<'de> for AreaCodeVisitor<'a> {
    type Value = &'a AreaCode;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        formatter.write_str("a string of two uppercase ASCII letters")
    }

    fn visit_borrowed_str<E>(self, v: &'a str) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        v.try_into().map_err(|error| match error {
            AreaCodeError::NotUppercaseOrDigit(byte) => E::invalid_value(
                serde::de::Unexpected::Char(byte.into()),
                &"only uppercase ASCII letters",
            ),
        })
    }
}

impl<'de: 'a, 'a> serde::Deserialize<'de> for &'a AreaCode {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_str(AreaCodeVisitor::<'a>::default())
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn correct() {
        assert!(<&AreaCode>::try_from("WIL").is_ok());
    }

    #[test]
    fn incorrect_capitalisation() {
        assert_eq!(
            <&AreaCode>::try_from("Gb"),
            Err(AreaCodeError::NotUppercaseOrDigit(b'b'))
        );
    }

    #[test]
    fn incorrect_ascii_type() {
        assert_eq!(
            <&AreaCode>::try_from("1,"),
            Err(AreaCodeError::NotUppercaseOrDigit(b','))
        );
    }

    #[test]
    fn round_trip() {
        assert_eq!(<&AreaCode>::try_from("001").unwrap().as_ref(), "001");
    }

    #[test]
    fn serialization() {
        let code = <&AreaCode>::try_from("BA").unwrap();
        let serialized = serde_json::to_string(&code).unwrap();
        assert_eq!(serialized, r#""BA""#);
    }

    #[test]
    fn deserialization() {
        let json = r#""ENG""#;
        let code = serde_json::from_str::<&AreaCode>(json).unwrap();
        assert_eq!(code, <&AreaCode>::try_from("ENG").unwrap());
    }
}
