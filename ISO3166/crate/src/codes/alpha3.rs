#[derive(Debug, Eq, thiserror::Error, PartialEq)]
pub enum Alpha3CodeError {
    #[error("expected string of length 3, not {0}")]
    IncorrectLength(usize),
    #[error("`{0}` does not represent an uppercase ASCII character")]
    NotUppercase(u8),
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Alpha3Code([u8; 3]);

impl Alpha3Code {
    // TODO: Get rid of this once `try_from` can be const.
    pub(crate) const fn new(value: &str) -> Result<Self, Alpha3CodeError> {
        let length = value.len();
        let bytes = value.as_bytes();

        if length != 3 {
            Err(Alpha3CodeError::IncorrectLength(length))
        } else if !bytes[0].is_ascii_uppercase() {
            Err(Alpha3CodeError::NotUppercase(bytes[0]))
        } else if !bytes[1].is_ascii_uppercase() {
            Err(Alpha3CodeError::NotUppercase(bytes[1]))
        } else if !bytes[2].is_ascii_uppercase() {
            Err(Alpha3CodeError::NotUppercase(bytes[2]))
        } else {
            Ok(Self([bytes[0], bytes[1], bytes[2]]))
        }
    }
}

impl TryFrom<&str> for Alpha3Code {
    type Error = Alpha3CodeError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        Alpha3Code::new(value)
    }
}

impl AsRef<str> for Alpha3Code {
    fn as_ref(&self) -> &str {
        std::str::from_utf8(&self.0).expect("Statically known to be valid UTF-8.")
    }
}

impl serde::Serialize for Alpha3Code {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(self.as_ref())
    }
}

struct Alpha3Visitor;

impl<'de> serde::de::Visitor<'de> for Alpha3Visitor {
    type Value = Alpha3Code;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        formatter.write_str("a string of three uppercase ASCII letters")
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        v.try_into().map_err(|error| match error {
            Alpha3CodeError::IncorrectLength(length) => {
                E::invalid_length(length, &"exactly three elements")
            }
            Alpha3CodeError::NotUppercase(byte) => E::invalid_value(
                serde::de::Unexpected::Char(byte.into()),
                &"only uppercase ASCII letters",
            ),
        })
    }
}

impl<'de> serde::Deserialize<'de> for Alpha3Code {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer.deserialize_str(Alpha3Visitor)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn correct() {
        assert_eq!(
            Alpha3Code::try_from("GBR"),
            Ok(Alpha3Code([b'G', b'B', b'R']))
        );
    }

    #[test]
    fn incorrect_length() {
        assert_eq!(
            Alpha3Code::try_from("GB"),
            Err(Alpha3CodeError::IncorrectLength(2))
        );
    }

    #[test]
    fn incorrect_capitalisation() {
        assert_eq!(
            Alpha3Code::try_from("GBr"),
            Err(Alpha3CodeError::NotUppercase(b'r'))
        );
    }

    #[test]
    fn incorrect_ascii_type() {
        assert_eq!(
            Alpha3Code::try_from("1BR"),
            Err(Alpha3CodeError::NotUppercase(b'1'))
        );
    }

    #[test]
    fn round_trip() {
        assert_eq!(Alpha3Code::try_from("ABW").unwrap().as_ref(), "ABW");
    }

    #[test]
    fn serialization() {
        let code = Alpha3Code::try_from("BEN").unwrap();
        let serialized = serde_json::to_string(&code).unwrap();
        assert_eq!(serialized, r#""BEN""#);
    }

    #[test]
    fn deserialization() {
        let json = r#""JOR""#;
        let code = serde_json::from_str::<Alpha3Code>(json).unwrap();
        assert_eq!(code, Alpha3Code::try_from("JOR").unwrap());
    }
}
