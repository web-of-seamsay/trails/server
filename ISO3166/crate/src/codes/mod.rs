mod alpha2;
mod alpha3;
mod area;
mod numeric;

pub use alpha2::{Alpha2Code, Alpha2CodeError};
pub use alpha3::{Alpha3Code, Alpha3CodeError};
pub use area::{AreaCode, AreaCodeError};
pub use numeric::{NumericCode, NumericCodeError};

fn serialize_byte_as_character<S>(byte: &u8, serializer: S) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
{
    serializer.serialize_char((*byte).into())
}
