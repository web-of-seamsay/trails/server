#[derive(Debug, Eq, thiserror::Error, PartialEq)]
pub enum NumericCodeError {
    #[error("expected string of length 3, not {0}")]
    IncorrectLength(usize),
    // TODO: Return all non-digit bytes.
    #[error("`{0}` does not represent an ASCII digit")]
    NotDigit(u8),
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct NumericCode([u8; 3]);

impl NumericCode {
    // TODO: Get rid of this once `try_from` can be const.
    pub(crate) const fn new(value: &str) -> Result<Self, NumericCodeError> {
        let length = value.len();
        let bytes = value.as_bytes();

        if length != 3 {
            Err(NumericCodeError::IncorrectLength(length))
        } else if !bytes[0].is_ascii_digit() {
            Err(NumericCodeError::NotDigit(bytes[0]))
        } else if !bytes[1].is_ascii_digit() {
            Err(NumericCodeError::NotDigit(bytes[1]))
        } else if !bytes[2].is_ascii_digit() {
            Err(NumericCodeError::NotDigit(bytes[2]))
        } else {
            Ok(NumericCode([bytes[0], bytes[1], bytes[2]]))
        }
    }
}

impl TryFrom<&str> for NumericCode {
    type Error = NumericCodeError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        NumericCode::new(value)
    }
}

impl AsRef<str> for NumericCode {
    fn as_ref(&self) -> &str {
        std::str::from_utf8(&self.0).expect("Statically known to be valid UTF-8.")
    }
}

impl serde::Serialize for NumericCode {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(self.as_ref())
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn correct() {
        assert_eq!(
            NumericCode::try_from("009"),
            Ok(NumericCode([b'0', b'0', b'9']))
        );
    }

    #[test]
    fn incorrect_length() {
        assert_eq!(
            NumericCode::try_from("00"),
            Err(NumericCodeError::IncorrectLength(2))
        );
    }

    #[test]
    fn incorrect_ascii_type() {
        assert_eq!(
            NumericCode::try_from("1BR"),
            Err(NumericCodeError::NotDigit(b'B'))
        );
    }

    #[test]
    fn round_trip() {
        assert_eq!(NumericCode::try_from("187").unwrap().as_ref(), "187");
    }

    #[test]
    fn serialization() {
        let code = NumericCode::try_from("999").unwrap();
        let serialized = serde_json::to_string(&code).unwrap();
        assert_eq!(serialized, r#""999""#);
    }
}
