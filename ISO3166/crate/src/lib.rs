mod codes;
mod data;

pub use codes::{Alpha2Code, Alpha2CodeError};
pub use codes::{Alpha3Code, Alpha3CodeError};
pub use codes::{AreaCode, AreaCodeError};
pub use codes::{NumericCode, NumericCodeError};
pub use data::{ALPHA2_CODE_AREA, ALPHA2_COUNTRY, ALPHA3_COUNTRY, COUNTRIES, NUMERIC_COUNTRY};

#[derive(Clone, Debug, serde::Serialize)]
pub struct Country {
    alpha2: Alpha2Code,
    alpha3: Alpha3Code,
    numeric: NumericCode,
    pub name: &'static str,
}

impl Country {
    pub fn find(query: &str) -> Option<Self> {
        ALPHA2_COUNTRY
            .get(query)
            .or_else(|| ALPHA3_COUNTRY.get(query))
            .or_else(|| NUMERIC_COUNTRY.get(query))
            .or_else(|| COUNTRIES.iter().find(|code| code.name == query))
            .cloned()
            .cloned()
    }

    pub fn alpha2(&self) -> &str {
        self.alpha2.as_ref()
    }

    pub fn alpha3(&self) -> &str {
        self.alpha3.as_ref()
    }

    pub fn numeric(&self) -> &str {
        self.numeric.as_ref()
    }
}

#[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
pub struct Area {
    pub code: &'static AreaCode,
    pub name: &'static str,
}

impl Area {
    pub fn find(country: &Country, query: &str) -> Option<Self> {
        let areas = ALPHA2_CODE_AREA
            .get(country.alpha2())
            .expect("Key should be statically verified.");

        areas
            .get(query)
            .or_else(|| areas.values().find(|area| area.name == query))
            .cloned()
            .cloned()
    }
}
