if __name__ != "__main__":
    raise Exception("This script can't be used as a module.")

import argparse
import json
import textwrap

parser = argparse.ArgumentParser(
    description="Generate a SQL script from the ISO 3166 JSON data file."
)
parser.add_argument("data", help="The input ISO 3166 JSON data file.")
parser.add_argument(
    "sql", help="The file path at which the script should be output."
)
parser.add_argument(
    "country", help="The file path at which the country CSV file should be output."
)
parser.add_argument(
    "area", help="The file path at which the area CSV file should be output."
)
parser.add_argument(
    "-l",
    "--log-level",
    default="info",
    help="The level at which to print log messages.",
)

arguments = parser.parse_args()

with open(arguments.data) as f:
    data = json.load(f)

with open(arguments.sql, "w") as f:
    print(".mode csv", file=f)
    print(f".import {arguments.country} country", file=f)
    print(f".import {arguments.area} area", file=f)

with open(arguments.country, "w") as f:
    print("alpha2", file=f)
    for alpha2 in sorted(data.keys()):
        print(alpha2, file=f)

with open(arguments.area, "w") as f:
    print("country,area", file=f)
    for (alpha2, country) in sorted(data.items(), key=lambda el: el[0]):
        for code in sorted(country["areas"].keys()):
            print(f"{alpha2},{code}", file=f)
