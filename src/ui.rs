use askama::Template;
use rocket::{http::Status, response::Debug};
use sqlx::Connection;

use crate::Either;

#[derive(Template)]
#[template(path = "editor.html")]
pub struct EditorTemplate {
    areas: Vec<(iso3166::Country, Vec<iso3166::Area>)>,
}

#[rocket::get("/ui/editor")]
pub async fn editor() -> EditorTemplate {
    let mut countries = iso3166::COUNTRIES.to_owned();
    countries.sort_unstable_by_key(|country| country.name);

    let areas = countries
        .into_iter()
        .cloned()
        .map(|country| {
            let mut areas = iso3166::ALPHA2_CODE_AREA[country.alpha2()]
                .values()
                .cloned()
                .cloned()
                .collect::<Vec<_>>();
            areas.sort_unstable_by_key(|area| area.name);

            (country, areas)
        })
        .collect();

    EditorTemplate { areas }
}

#[derive(Template)]
#[template(path = "countries.html")]
pub struct CountriesTemplate {
    countries: Vec<iso3166::Country>,
}

#[rocket::get("/ui/trails")]
pub async fn countries(
    mut db: rocket_db_pools::Connection<crate::Main>,
) -> Result<CountriesTemplate, Debug<sqlx::Error>> {
    let mut countries = sqlx::query!("SELECT DISTINCT country FROM trail")
        .fetch_all(&mut *db)
        .await?
        .into_iter()
        .map(|record| {
            iso3166::ALPHA2_COUNTRY
                .get(&record.country)
                .expect("Invalid country codes shouldn't make it into the DB.")
        })
        .cloned()
        .cloned()
        .collect::<Vec<_>>();

    countries.sort_unstable_by_key(|country| country.name);

    Ok(CountriesTemplate { countries })
}

#[derive(Clone, rocket::form::FromForm)]
pub struct CoordinateForm {
    order: usize,
    latitude: f64,
    longitude: f64,
}

// TODO: Can these be string slices instead? Nah, just impl `FromForm` for the country and area.
#[derive(rocket::form::FromForm)]
pub struct NewTrailForm<'a> {
    country: crate::Alpha2Code,
    area: crate::AreaCode<'a>,
    name: String,
    coordinates: Vec<CoordinateForm>,
}

#[rocket::post("/ui/trails", data = "<form>")]
pub async fn new_trail(
    mut db: rocket_db_pools::Connection<crate::Main>,
    form: rocket::form::Form<rocket::form::Strict<NewTrailForm<'_>>>,
) -> Result<rocket::response::Redirect, Either<Status, Debug<sqlx::Error>>> {
    let crate::Alpha2Code(country) = &form.country;
    let country = iso3166::Country::find(country.as_ref()).ok_or(Either::Left(Status::NotFound))?;

    let crate::AreaCode(area) = form.area;
    let area =
        iso3166::Area::find(&country, area.as_ref()).ok_or(Either::Left(Status::NotFound))?;
    let id = uuid::Uuid::new_v4();

    {
        let country = country.clone();
        let area = area.clone();
        // TODO: Are these really necessary?
        let name = form.name.clone();
        let coordinates = form.coordinates.clone();

        db.transaction(|connection| {
            Box::pin(async move {
                let alpha2 = country.alpha2();
                let area_code = area.code.as_ref();
                let id_str = id.to_string();

                sqlx::query!(
                    "INSERT INTO trail (country, area, id, name) VALUES (?, ?, ?, ?)",
                    alpha2,
                    area_code,
                    id_str,
                    name,
                )
                .execute(&mut *connection)
                .await?;

                for coordinate in coordinates {
                    let order = i64::try_from(coordinate.order)
                        .expect("We can't realistically have this many coordinates in a route.");
                    sqlx::query!(
                        r#"
                        INSERT INTO
                            trail_coordinate (
                                country,
                                area,
                                id,
                                "order",
                                latitude,
                                longitude
                            )
                        VALUES (
                            ?,
                            ?,
                            ?,
                            ?,
                            ?,
                            ?
                        )
                    "#,
                        alpha2,
                        area_code,
                        id_str,
                        order,
                        coordinate.latitude,
                        coordinate.longitude,
                    )
                    .execute(&mut *connection)
                    .await?;
                }

                Ok::<_, sqlx::Error>(())
            })
        })
        .await
        .map_err(Either::right)?;
    }

    Ok(rocket::response::Redirect::to(rocket::uri!(trail(
        country = country.alpha2(),
        area = area.code.as_ref(),
        id = id
    ))))
}

#[derive(Template)]
#[template(path = "areas.html")]
pub struct AreasTemplate {
    country: iso3166::Country,
    areas: Vec<iso3166::Area>,
}

#[rocket::get("/ui/trails/<country>")]
pub async fn areas(
    mut db: rocket_db_pools::Connection<crate::Main>,
    country: &str,
) -> Result<AreasTemplate, Either<Status, Debug<sqlx::Error>>> {
    let country = iso3166::Country::find(country).ok_or(Either::Left(Status::NotFound))?;
    let alpha2 = country.alpha2();

    let mut areas = sqlx::query!("SELECT DISTINCT area FROM trail WHERE country = ?", alpha2,)
        .fetch_all(&mut *db)
        .await
        .map_err(Either::right)?
        .into_iter()
        .map(|record| {
            iso3166::ALPHA2_CODE_AREA
                .get(country.alpha2())
                .expect("Invalid country codes should never make it into the database.")
                .get(&record.area)
                .expect("Invalid country-area pairs should never make it into the database.")
        })
        .cloned()
        .cloned()
        .collect::<Vec<_>>();

    areas.sort_unstable_by_key(|area| area.name);

    Ok(AreasTemplate { country, areas })
}

#[derive(Clone, Debug)]
struct TrailInfo {
    id: uuid::Uuid,
    name: String,
}

#[derive(Template)]
#[template(path = "trails.html")]
pub struct TrailsTemplate {
    country: iso3166::Country,
    area: iso3166::Area,
    trails: Vec<TrailInfo>,
}

#[rocket::get("/ui/trails/<country>/<area>")]
pub async fn trails(
    mut db: rocket_db_pools::Connection<crate::Main>,
    country: &str,
    area: &str,
) -> Result<TrailsTemplate, Either<Status, Debug<sqlx::Error>>> {
    // TODO: Make this a fairing too.
    let country = iso3166::Country::find(country).ok_or(Either::Left(Status::NotFound))?;
    let area = iso3166::Area::find(&country, area).ok_or(Either::Left(Status::NotFound))?;

    let alpha2 = country.alpha2();
    let area_code = area.code.as_ref();
    let trails = sqlx::query!(
        "SELECT id, name FROM trail WHERE country = ? AND area = ? ORDER BY name",
        alpha2,
        area_code,
    )
    .fetch_all(&mut *db)
    .await
    .map_err(Either::right)?
    .into_iter()
    .map(|record| TrailInfo {
        id: uuid::Uuid::parse_str(&record.id)
            .expect("Invalid UUIDs should never make it into the database."),
        name: record.name,
    })
    .collect();

    Ok(TrailsTemplate {
        country,
        area,
        trails,
    })
}

#[derive(rocket::serde::Serialize)]
#[serde(crate = "rocket::serde")]
pub struct LeafletCoordinate {
    #[serde(alias = "lat", rename(serialize = "lat"))]
    latitude: f64,
    #[serde(alias = "lng", rename(serialize = "lng"))]
    longitude: f64,
}

// TODO: I shouldn't need to store the local _and_ UTC dates, but trying to convert in the templates causes lifetime errors.
#[derive(rocket::serde::Serialize)]
#[serde(crate = "rocket::serde")]
pub struct ExcursionDates {
    start_utc: chrono::DateTime<chrono::Utc>,
    start_local: chrono::DateTime<chrono::FixedOffset>,
    end_utc: chrono::DateTime<chrono::Utc>,
    end_local: chrono::DateTime<chrono::FixedOffset>,
}

#[derive(Template)]
#[template(path = "trail.html")]
pub struct TrailTemplate {
    country: iso3166::Country,
    area: iso3166::Area,
    info: TrailInfo,
    coordinates: Vec<LeafletCoordinate>,
    excursions: Vec<ExcursionDates>,
}

#[rocket::get("/ui/trails/<country>/<area>/<id>")]
pub async fn trail(
    mut db: rocket_db_pools::Connection<crate::Main>,
    country: &str,
    area: &str,
    id: uuid::Uuid,
) -> Result<TrailTemplate, Either<Status, Debug<sqlx::Error>>> {
    let country = iso3166::Country::find(country).ok_or(Either::Left(Status::NotFound))?;
    let area = iso3166::Area::find(&country, area).ok_or(Either::Left(Status::NotFound))?;

    let alpha2 = country.alpha2();
    let area_code = area.code.as_ref();
    let id_str = id.to_string();

    let name = sqlx::query!(
        "SELECT name FROM trail WHERE country = ? AND area = ? AND id = ?",
        alpha2,
        area_code,
        id_str,
    )
    .fetch_one(&mut *db)
    .await
    .map_err(Either::right)?
    .name;

    let coordinates = sqlx::query!(
        r#"
            SELECT
                latitude as "latitude: f64",
                longitude as "longitude: f64"
            FROM
                trail_coordinate
            WHERE
                country = ? AND
                area = ? AND
                id = ?
            ORDER BY
                "order"
        "#,
        alpha2,
        area_code,
        id_str,
    )
    .fetch_all(&mut *db)
    .await
    .map_err(Either::right)?
    .into_iter()
    .map(|record| LeafletCoordinate {
        latitude: record.latitude,
        longitude: record.longitude,
    })
    .collect();

    let excursions = sqlx::query!(
        r#"
            SELECT
                min(excursion_coordinate."timestamp") as "start: chrono::DateTime<chrono::FixedOffset>",
                max(excursion_coordinate."timestamp") as "end: chrono::DateTime<chrono::FixedOffset>"
            FROM
                trail_excursion
            LEFT JOIN
                excursion_coordinate
            ON
                trail_excursion.excursion_id = excursion_coordinate.id
            WHERE
                trail_excursion.country = ? AND
                trail_excursion.area = ? AND
                trail_excursion.trail_id = ?
            GROUP BY
                excursion_coordinate.id
        "#,
        alpha2,
        area_code,
        id_str,
    )
    .fetch_all(&mut *db)
    .await
    .map_err(Either::right)?
    .into_iter()
    .map(|record| {
        let start_local = record.start.unwrap();
        let start_utc = start_local.with_timezone(&chrono::Utc);

        let end_local = record.end.unwrap();
        let end_utc = end_local.with_timezone(&chrono::Utc);

        ExcursionDates {
            start_utc,
            start_local,
            end_utc,
            end_local,
        }
    })
    .collect();

    Ok(TrailTemplate {
        country,
        area,
        info: TrailInfo { id, name },
        coordinates,
        excursions,
    })
}
