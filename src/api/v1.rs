use crate::Either;
use rocket::{
    response::{status::BadRequest, Debug},
    serde::json,
};
use sqlx::Connection;

// TODO: Test that the produced formatted error matches serde's.
#[derive(Clone, Debug, thiserror::Error, rocket::serde::Serialize)]
#[serde(crate = "rocket::serde")]
pub enum ParseErrorKind {
    #[error("expected `{0}`")]
    Expected(String),
    #[error("invalid length: {length}, expected {expected}")]
    InvalidLength { length: usize, expected: String },
    // TODO: Can these use proper types instead of just stringifying everything?
    #[error("invalid type: {actual}, expected {expected}")]
    InvalidType { actual: String, expected: String },
    #[error("missing field `{0}`")]
    MissingField(String),
    #[error("premature end of input")]
    PrematureEndOfInput,
}

static RGX_INNER_EXPECTED: once_cell::sync::Lazy<regex::Regex> = once_cell::sync::Lazy::new(|| {
    regex::Regex::new(r"^expected (?P<expected>.+)$").expect("Statically correct regex.")
});

static RGX_INNER_INVALID_LENGTH: once_cell::sync::Lazy<regex::Regex> =
    once_cell::sync::Lazy::new(|| {
        regex::Regex::new(r"^invalid length (?P<length>\d+), expected (?P<expected>.+)$")
            .expect("Statically correct regex.")
    });

static RGX_INNER_INVALID_TYPE: once_cell::sync::Lazy<regex::Regex> =
    once_cell::sync::Lazy::new(|| {
        regex::Regex::new(r"^invalid type: (?P<actual>[^,]+), expected (?P<expected>.+)$")
            .expect("Statically correct regex.")
    });

static RGX_INNER_MISSING_FIELD: once_cell::sync::Lazy<regex::Regex> =
    once_cell::sync::Lazy::new(|| {
        regex::Regex::new(r"^missing field `(?P<expected>[^`]+)`$")
            .expect("Statically correct regex.")
    });

impl From<&str> for ParseErrorKind {
    fn from(error: &str) -> Self {
        // NOTE: Cheaper options first.
        if error == "premature end of input" {
            Self::PrematureEndOfInput
        } else if let Some(captures) = RGX_INNER_EXPECTED.captures(error) {
            let expected = captures["expected"].into();

            Self::Expected(expected)
        } else if let Some(captures) = RGX_INNER_MISSING_FIELD.captures(error) {
            let expected = captures["expected"].into();

            Self::MissingField(expected)
        } else if let Some(captures) = RGX_INNER_INVALID_LENGTH.captures(error) {
            let length = captures["length"]
                .parse()
                .expect("Length must be a valid positive integer.");
            let expected = captures["expected"].into();

            Self::InvalidLength { length, expected }
        } else if let Some(captures) = RGX_INNER_INVALID_TYPE.captures(error) {
            let actual = captures["actual"].into();
            let expected = captures["expected"].into();

            Self::InvalidType { actual, expected }
        } else {
            unimplemented!("errors of form `{}` have not yet been implemented", error);
        }
    }
}

#[derive(Clone, Debug, thiserror::Error, rocket::serde::Serialize)]
#[serde(crate = "rocket::serde")]
#[error("{kind} at line {line} column {column}")]
pub struct ParseError {
    line: usize,
    column: usize,
    kind: ParseErrorKind,
}

static RGX_OUTER: once_cell::sync::Lazy<regex::Regex> = once_cell::sync::Lazy::new(|| {
    regex::Regex::new(
        r"^parse error:\s+(?P<inner>.*)\s+at line (?P<line>\d+) column (?P<column>\d+)$",
    )
    .expect("Statically correct regex.")
});

impl From<&str> for ParseError {
    fn from(error: &str) -> Self {
        let captures = RGX_OUTER
            .captures(error)
            .expect("The error must match the regex.");

        let line = captures["line"]
            .parse()
            .expect("Line must be a valid positive integer.");
        let column = captures["column"]
            .parse()
            .expect("Column must be a valid positive integer.");
        let kind = captures["inner"].into();

        ParseError { line, column, kind }
    }
}

impl<'r> From<json::Error<'r>> for ParseError {
    fn from(error: json::Error<'r>) -> Self {
        ParseError::from(format!("{}", error).as_str())
    }
}

#[derive(Debug, rocket::serde::Deserialize, rocket::serde::Serialize)]
#[serde(crate = "rocket::serde")]
pub struct TrailKey<'a> {
    country: iso3166::Alpha2Code,
    #[serde(borrow)]
    area: &'a iso3166::AreaCode,
    id: uuid::Uuid,
}

#[derive(Debug, rocket::serde::Deserialize, rocket::serde::Serialize)]
#[serde(crate = "rocket::serde")]
pub struct TrailInfo<'a> {
    #[serde(borrow)]
    key: TrailKey<'a>,
    name: String,
}

#[rocket::get("/api/v1/trails")]
pub async fn trails(
    mut db: rocket_db_pools::Connection<crate::Main>,
) -> Result<json::Json<Vec<TrailInfo<'static>>>, Debug<sqlx::Error>> {
    let trails = sqlx::query!(r#"SELECT country, area, id, name FROM trail"#)
        .fetch_all(&mut *db)
        .await?
        .into_iter()
        .map(|record| {
            let country = record.country.as_str();
            assert!(iso3166::ALPHA2_COUNTRY.contains_key(country));
            let country: iso3166::Alpha2Code = country
                .try_into()
                .expect("Invalid country codes should never make it into the database.");

            let area = record.area.as_str();
            let areas = iso3166::ALPHA2_CODE_AREA
                .get(country.as_ref())
                .expect("Invalid country codes should never make it into the database.");
            assert!(areas.contains_key(area));
            // Get a _static_ reference to the area code string.
            let area = (*areas
                .get_key(area)
                .expect("Invalid area codes should never make it into the database."))
            .try_into()
            .expect("Invalid area codes should _definitely_ never make it into the static data.");

            let id = uuid::Uuid::parse_str(&record.id)
                .expect("Invalid UUIDs should never make it into the database.");

            TrailInfo {
                key: TrailKey { country, area, id },
                name: record.name,
            }
        })
        .collect();

    Ok(json::Json(trails))
}

fn serialize_error<E, S>(error: &E, serializer: S) -> Result<S::Ok, S::Error>
where
    E: std::fmt::Display,
    S: rocket::serde::Serializer,
{
    serializer.serialize_str(&format!("{error}"))
}

#[derive(Debug, thiserror::Error, rocket::serde::Serialize)]
#[serde(crate = "rocket::serde")]
pub enum InvalidTrailKey<'r> {
    #[error(transparent)]
    InvalidCountry(#[from] iso3166::Alpha2CodeError),
    #[error("`{}` does not match any known country codes", .0.as_ref())]
    UnknownCountry(iso3166::Alpha2Code),
    #[error("there is no area `{}` in country `{}`", .area, .country.as_ref())]
    UnknownArea {
        country: iso3166::Alpha2Code,
        area: &'r str,
    },
    #[serde(serialize_with = "serialize_error")]
    #[error(transparent)]
    InvalidId(#[from] uuid::Error),
    #[error("there is no trail with id `{}` in area `{}` of country `{}`", .id, .area, .country.as_ref())]
    UnknownId {
        country: iso3166::Alpha2Code,
        area: &'r str,
        id: uuid::Uuid,
    },
}

#[derive(Clone, Debug, rocket::serde::Deserialize, rocket::serde::Serialize)]
#[serde(crate = "rocket::serde")]
pub struct Coordinate {
    latitude: f64,
    longitude: f64,
}

#[rocket::get("/api/v1/route/<country>/<area>/<id>")]
pub async fn route(
    mut db: rocket_db_pools::Connection<crate::Main>,
    country: Result<crate::Alpha2Code, iso3166::Alpha2CodeError>,
    area: &str,
    id: Result<crate::Uuid, uuid::Error>,
) -> Result<
    json::Json<Vec<Coordinate>>,
    Either<BadRequest<json::Json<InvalidTrailKey<'_>>>, Debug<sqlx::Error>>,
> {
    let crate::Alpha2Code(country) = country
        .map_err(InvalidTrailKey::InvalidCountry)
        .map_err(json::Json)
        .map_err(Some)
        .map_err(BadRequest)
        .map_err(Either::Left)?;

    let country_str = iso3166::ALPHA2_COUNTRY
        .get_key(country.as_ref())
        .ok_or_else(|| country.clone())
        .map_err(InvalidTrailKey::UnknownCountry)
        .map_err(json::Json)
        .map_err(Some)
        .map_err(BadRequest)
        .map_err(Either::Left)?;

    let area = iso3166::ALPHA2_CODE_AREA
        .get(country_str)
        .expect("Already ensured country code is valid.")
        .get_key(area)
        .ok_or_else(|| InvalidTrailKey::UnknownArea {
            country: country.clone(),
            area,
        })
        .map_err(json::Json)
        .map_err(Some)
        .map_err(BadRequest)
        .map_err(Either::Left)?;

    let crate::Uuid(id) = id
        .map_err(InvalidTrailKey::InvalidId)
        .map_err(json::Json)
        .map_err(Some)
        .map_err(BadRequest)
        .map_err(Either::Left)?;

    let id_str = id.to_string();

    sqlx::query!(
        r#"
            SELECT DISTINCT
                id
            FROM
                trail
            WHERE
                id = ?
        "#,
        id_str
    )
    .fetch_optional(&mut *db)
    .await
    .map_err(Either::right)?
    .ok_or_else(|| InvalidTrailKey::UnknownId {
        country: country.clone(),
        area,
        id,
    })
    .map_err(json::Json)
    .map_err(Some)
    .map_err(BadRequest)
    .map_err(Either::Left)?;

    let coordinates = sqlx::query!(
        r#"
            SELECT
                latitude as "latitude: f64",
                longitude as "longitude: f64"
            FROM
                trail_coordinate
            WHERE
                country = ? AND
                area = ? AND
                id = ?
            ORDER BY "order"
        "#,
        country_str,
        area,
        id_str,
    )
    .fetch_all(&mut *db)
    .await
    .map_err(Either::right)?
    .into_iter()
    .map(|record| Coordinate {
        latitude: record.latitude,
        longitude: record.longitude,
    })
    .collect();

    Ok(json::Json(coordinates))
}

#[derive(Clone, Debug, rocket::serde::Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct ExcursionCoordinate {
    timestamp: chrono::DateTime<chrono::FixedOffset>,
    coordinate: Coordinate,
}

// TODO: Produce machine readable errors when unable to parse JSON.
#[rocket::post("/api/v1/excursion", data = "<data>", format = "application/json")]
pub async fn new_excursion(
    mut db: rocket_db_pools::Connection<crate::Main>,
    data: Result<json::Json<Vec<ExcursionCoordinate>>, json::Error<'_>>,
) -> Result<json::Json<uuid::Uuid>, Either<BadRequest<json::Json<ParseError>>, Debug<sqlx::Error>>>
{
    let id = uuid::Uuid::new_v4();
    let json::Json(data) = data
        .map_err(ParseError::from)
        .map_err(json::Json)
        .map_err(Some)
        .map_err(BadRequest)
        .map_err(Either::Left)?;

    db.transaction(|connection| {
        Box::pin(async move {
            let id_str = id.to_string();

            sqlx::query!(r#"INSERT INTO excursion (id) VALUES (?)"#, id_str)
                .execute(&mut *connection)
                .await?;

            for coordinate in &data {
                sqlx::query!(
                    r#"
                        INSERT INTO
                            excursion_coordinate (
                                id,
                                "timestamp",
                                latitude,
                                longitude
                            )
                        VALUES (
                            ?,
                            ?,
                            ?,
                            ?
                        )
                    "#,
                    id_str,
                    coordinate.timestamp,
                    coordinate.coordinate.latitude,
                    coordinate.coordinate.longitude,
                )
                .execute(&mut *connection)
                .await?;
            }

            Ok::<_, sqlx::Error>(())
        })
    })
    .await
    .map_err(Either::right)?;

    Ok(json::Json(id))
}
