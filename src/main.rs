// TODO: Splitting ui and api into separate crates may improve compile times.
mod api;
mod ui;

use rocket_db_pools::Database;

#[derive(Database, Debug)]
#[database("main")]
pub struct Main(rocket_db_pools::sqlx::SqlitePool);

#[derive(Debug)]
pub enum Either<L, R> {
    Left(L),
    Right(R),
}

impl<L, R> Either<L, R> {
    pub fn left<T: Into<L>>(value: T) -> Self {
        Either::Left(value.into())
    }

    pub fn right<T: Into<R>>(value: T) -> Self {
        Either::Right(value.into())
    }
}

impl<
        'r,
        'o: 'r,
        L: rocket::response::Responder<'r, 'o>,
        R: rocket::response::Responder<'r, 'o>,
    > rocket::response::Responder<'r, 'o> for Either<L, R>
{
    fn respond_to(self, request: &'r askama_rocket::Request<'_>) -> rocket::response::Result<'o> {
        rocket::Response::build_from(match self {
            Either::Left(left) => left.respond_to(request)?,
            Either::Right(right) => right.respond_to(request)?,
        })
        .ok()
    }
}

#[derive(Clone, Debug)]
pub struct Alpha2Code(pub iso3166::Alpha2Code);

impl<'r> rocket::request::FromParam<'r> for Alpha2Code {
    type Error = iso3166::Alpha2CodeError;

    fn from_param(param: &'r str) -> Result<Self, Self::Error> {
        param.try_into().map(Alpha2Code)
    }
}

#[rocket::async_trait]
impl<'r> rocket::form::FromFormField<'r> for Alpha2Code {
    fn from_value(field: rocket::form::ValueField<'r>) -> rocket::form::Result<'r, Self> {
        field
            .value
            .try_into()
            .map(Alpha2Code)
            .map_err(rocket::form::Error::custom)
            .map_err(From::from)
    }
}

#[derive(Clone, Debug)]
pub struct AreaCode<'a>(pub &'a iso3166::AreaCode);

#[rocket::async_trait]
impl<'a, 'r: 'a> rocket::form::FromFormField<'r> for AreaCode<'a> {
    fn from_value(field: rocket::form::ValueField<'r>) -> rocket::form::Result<'r, Self> {
        field
            .value
            .try_into()
            .map(AreaCode)
            .map_err(rocket::form::Error::custom)
            .map_err(From::from)
    }
}

#[derive(Clone, Debug)]
pub struct Uuid(pub uuid::Uuid);

impl<'r> rocket::request::FromParam<'r> for Uuid {
    type Error = uuid::Error;

    fn from_param(param: &'r str) -> Result<Self, Self::Error> {
        param.parse().map(Uuid)
    }
}

#[rocket::launch]
fn rocket() -> _ {
    let rocket = rocket::build();
    let figment = rocket.figment();

    let stylesheets: std::path::PathBuf = figment
        .extract_inner("stylesheets")
        .expect("Don't want to start without stylesheets...");
    let scripts: std::path::PathBuf = figment
        .extract_inner("scripts")
        .expect("Don't want to start without scripts...");

    rocket
        .attach(Main::init())
        .mount(
            "/",
            rocket::routes![
                index,
                api::v1::trails,
                api::v1::route,
                api::v1::new_excursion,
                ui::editor,
                ui::new_trail,
                ui::countries,
                ui::areas,
                ui::trails,
                ui::trail,
            ],
        )
        .mount("/style", rocket::fs::FileServer::from(stylesheets))
        .mount("/scripts", rocket::fs::FileServer::from(scripts))
}

#[rocket::get("/")]
async fn index() -> rocket::response::Redirect {
    rocket::response::Redirect::to(rocket::uri!(ui::countries))
}
