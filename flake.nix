{
  inputs = {
    naersk.url = "github:nmattia/naersk/master";
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils, naersk }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        naersk-lib = pkgs.callPackage naersk { };
        nativeBuildInputs = with pkgs; [
          fd
          openssl
          pkg-config
          (python3.withPackages
            (py: with py; [ beautifulsoup4 pyparsing requests ]))
          sqlite
        ];
      in {
        defaultPackage = naersk-lib.buildPackage {
          root = ./.;

          inherit nativeBuildInputs;
        };

        defaultApp = let
          drv = self.defaultPackage."${system}";
          name = pkgs.lib.strings.removeSuffix ("-" + drv.version) drv.name;
        in utils.lib.mkApp {
          inherit drv;
          # TODO: https://github.com/nix-community/naersk/issues/224
          exePath = "/bin/${name}";
        };

        devShell = with pkgs;
          mkShell {
            buildInputs = [
              asciidoctor
              black
              cargo
              rust-analyzer
              rustc
              rustfmt
              rustPackages.clippy
            ];
            inherit nativeBuildInputs;

            DATABASE_URL = "sqlite:db/test.sqlite";
            RUST_SRC_PATH = rustPlatform.rustLibSrc;
          };
      });
}
