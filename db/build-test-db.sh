#!/usr/bin/env bash

set -euo pipefail

rm -f "$1"

scripts="$(realpath "$(dirname "$0")/scripts")"

for script in $(find "$scripts" -type f -name '*.sql' | sort)
do
    sqlite3 "$1" < "$script"
done

sqlite3 "$1" <<EOF
INSERT INTO
    trail (
        country,
        area,
        id,
        name
    )
VALUES
    (
        'GB',
        'WIL',
        'a36a1df0-4d8a-4e22-a495-fccfb40d5078',
        'Cherhill Downs'
    );

INSERT INTO
    trail_coordinate (
        country,
        area,
        id,
        "order",
        latitude,
        longitude
    )
VALUES
    (
        'GB',
        'WIL',
        'a36a1df0-4d8a-4e22-a495-fccfb40d5078',
        0,
        51.430329,
        -1.935040
    );

INSERT INTO
    trail_coordinate (
        country,
        area,
        id,
        "order",
        latitude,
        longitude
    )
VALUES
    (
        'GB',
        'WIL',
        'a36a1df0-4d8a-4e22-a495-fccfb40d5078',
        1,
        51.429660,
        -1.934943
    );

INSERT INTO
    trail_coordinate (
        country,
        area,
        id,
        "order",
        latitude,
        longitude
    )
VALUES
    (
        'GB',
        'WIL',
        'a36a1df0-4d8a-4e22-a495-fccfb40d5078',
        2,
        51.428068,
        -1.933216
    );

INSERT INTO
    trail_coordinate (
        country,
        area,
        id,
        "order",
        latitude,
        longitude
    )
VALUES
    (
        'GB',
        'WIL',
        'a36a1df0-4d8a-4e22-a495-fccfb40d5078',
        3,
        51.427901,
        -1.932465
    );

INSERT INTO
    trail_coordinate (
        country,
        area,
        id,
        "order",
        latitude,
        longitude
    )
VALUES
    (
        'GB',
        'WIL',
        'a36a1df0-4d8a-4e22-a495-fccfb40d5078',
        4,
        51.426389,
        -1.929729
    );

INSERT INTO
    trail_coordinate (
        country,
        area,
        id,
        "order",
        latitude,
        longitude
    )
VALUES
    (
        'GB',
        'WIL',
        'a36a1df0-4d8a-4e22-a495-fccfb40d5078',
        5,
        51.424913,
        -1.929350
    );

INSERT INTO
    trail_coordinate (
        country,
        area,
        id,
        "order",
        latitude,
        longitude
    )
VALUES
    (
        'GB',
        'WIL',
        'a36a1df0-4d8a-4e22-a495-fccfb40d5078',
        6,
        51.422846,
        -1.932182
    );

INSERT INTO
    trail_coordinate (
        country,
        area,
        id,
        "order",
        latitude,
        longitude
    )
VALUES
    (
        'GB',
        'WIL',
        'a36a1df0-4d8a-4e22-a495-fccfb40d5078',
        7,
        51.422779,
        -1.935336
    );

INSERT INTO
    trail_coordinate (
        country,
        area,
        id,
        "order",
        latitude,
        longitude
    )
VALUES
    (
        'GB',
        'WIL',
        'a36a1df0-4d8a-4e22-a495-fccfb40d5078',
        8,
        51.423595,
        -1.939359
    );

INSERT INTO
    trail_coordinate (
        country,
        area,
        id,
        "order",
        latitude,
        longitude
    )
VALUES
    (
        'GB',
        'WIL',
        'a36a1df0-4d8a-4e22-a495-fccfb40d5078',
        9,
        51.428113,
        -1.942893
    );

INSERT INTO
    trail_coordinate (
        country,
        area,
        id,
        "order",
        latitude,
        longitude
    )
VALUES
    (
        'GB',
        'WIL',
        'a36a1df0-4d8a-4e22-a495-fccfb40d5078',
        10,
        51.429538,
        -1.943011
    );

INSERT INTO
    excursion (
        id
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9'
    );

INSERT INTO
    trail_excursion (
        country,
        area,
        trail_id,
        excursion_id
    )
VALUES
    (
        'GB',
        'WIL',
        'a36a1df0-4d8a-4e22-a495-fccfb40d5078',
        '6bf54028-99f2-4333-bf05-0152119fbed9'
    );

INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:01+00:00',
        51.430369887718,
        -1.93500995635986
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:02+00:00',
        51.4301625264938,
        -1.93500995635986
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:03+00:00',
        51.4299618534452,
        -1.93500995635986
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:04+00:00',
        51.4296407747343,
        -1.93495631217957
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:05+00:00',
        51.4293865858213,
        -1.93478465080261
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:06+00:00',
        51.4291390847314,
        -1.93447351455688
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:07+00:00',
        51.4288514466452,
        -1.93407654762268
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:08+00:00',
        51.4284902241581,
        -1.93365812301636
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:09+00:00',
        51.4282092713606,
        -1.93329334259033
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:10+00:00',
        51.428108930657,
        -1.93323969841003
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:11+00:00',
        51.4279550744837,
        -1.93291783332825
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:12+00:00',
        51.4278881803337,
        -1.93262815475464
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:13+00:00',
        51.4277945283592,
        -1.93225264549255
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:14+00:00',
        51.4274600554547,
        -1.93180203437805
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:15+00:00',
        51.4271790963228,
        -1.93134069442749
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:16+00:00',
        51.4269115145911,
        -1.93082571029663
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:17+00:00',
        51.4267108272641,
        -1.93037509918213
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:18+00:00',
        51.4264432427902,
        -1.92984938621521
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:19+00:00',
        51.4262760016984,
        -1.92967772483826
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:20+00:00',
        51.4259548970862,
        -1.92952752113342
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:21+00:00',
        51.4255802721869,
        -1.92934513092041
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:22+00:00',
        51.4250517782672,
        -1.92922711372376
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:23+00:00',
        51.4246570762897,
        -1.9296133518219
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:24+00:00',
        51.4244697589041,
        -1.93007469177246
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:25+00:00',
        51.4242222311812,
        -1.93035364151001
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:26+00:00',
        51.4239947720916,
        -1.93089008331299
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:27+00:00',
        51.4232722474736,
        -1.93137288093567
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:28+00:00',
        51.4229109808809,
        -1.93224191665649
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:29+00:00',
        51.422917671029,
        -1.93275690078735
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:30+00:00',
        51.4228976005819,
        -1.93301439285278
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:31+00:00',
        51.4227303465136,
        -1.93323969841003
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:32+00:00',
        51.4226099232055,
        -1.93436622619629
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:33+00:00',
        51.4229444316113,
        -1.93590044975281
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:34+00:00',
        51.4231317552491,
        -1.93653345108032
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:35+00:00',
        51.4233123880298,
        -1.9375741481781
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:36+00:00',
        51.423432809487,
        -1.93853974342346
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:37+00:00',
        51.4236000609845,
        -1.93952679634094
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:38+00:00',
        51.4236000609845,
        -1.9396984577179
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:39+00:00',
        51.4239747021175,
        -1.93973064422607
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:40+00:00',
        51.4243894797895,
        -1.94013833999634
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:41+00:00',
        51.4245901773115,
        -1.94040656089783
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:42+00:00',
        51.4249581204794,
        -1.94073915481567
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:43+00:00',
        51.4255267540934,
        -1.94128632545471
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:44+00:00',
        51.4257006876681,
        -1.94137215614319
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:45+00:00',
        51.4260686218945,
        -1.9415545463562
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:46+00:00',
        51.4270118579241,
        -1.94224119186401
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:47+00:00',
        51.4273730920985,
        -1.94253087043762
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:48+00:00',
        51.4276005343728,
        -1.94263815879822
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:49+00:00',
        51.427908248589,
        -1.94282054901123
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:50+00:00',
        51.4281691351056,
        -1.94283127784729
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:51+00:00',
        51.4285972533409,
        -1.94298148155212
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:52+00:00',
        51.4290253675649,
        -1.9430673122406
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:53+00:00',
        51.4293732074183,
        -1.9430136680603
    );
INSERT INTO
    excursion_coordinate (
        id,
        "timestamp",
        latitude,
        longitude
    )
VALUES
    (
        '6bf54028-99f2-4333-bf05-0152119fbed9',
        '2022-07-07 16:54+00:00',
        51.4296073289055,
        -1.94296002388001
    );
EOF