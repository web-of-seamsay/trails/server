CREATE TABLE country (
    alpha2 TEXT UNIQUE NOT NULL
);

CREATE TABLE area (
    country TEXT NOT NULL,
    area TEXT NOT NULL,
    UNIQUE (country, area),
    FOREIGN KEY (country) REFERENCES country (alpha2)
);

CREATE TABLE trail (
    country TEXT NOT NULL,
    area TEXT NOT NULL,
    id TEXT NOT NULL,
    name TEXT NOT NULL,
    PRIMARY KEY (country, area, id),
    FOREIGN KEY (country, area) REFERENCES area (country, area)
);

CREATE TABLE trail_coordinate (
    country TEXT NOT NULL,
    area TEXT NOT NULL,
    id TEXT NOT NULL,
    "order" INTEGER NOT NULL,
    latitude REAL NOT NULL,
    longitude REAL NOT NULL,
    PRIMARY KEY (country, area, id, "order"),
    FOREIGN KEY (country, area, id) REFERENCES trail (country, area, id)
);

CREATE TABLE excursion (
    id TEXT NOT NULL PRIMARY KEY
);

CREATE TABLE trail_excursion (
    country TEXT NOT NULL,
    area TEXT NOT NULL,
    trail_id TEXT NOT NULL,
    excursion_id TEXT NOT NULL,
    FOREIGN KEY (country, area, trail_id) REFERENCES trail (country, area, id),
    FOREIGN KEY (excursion_id) REFERENCES excursion (id)
);

CREATE TABLE excursion_coordinate (
    id TEXT NOT NULL,
    "timestamp" DATETIME NOT NULL,
    latitude REAL NOT NULL,
    longitude REAL NOT NULL,
    PRIMARY KEY (id, "timestamp"),
    FOREIGN KEY (id) REFERENCES excursion
);