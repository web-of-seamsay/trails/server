rust_source_files := $(shell fd '\.rs$$' src)
html_template_files := $(shell fd '\.html$$' templates)

.PHONY: run
run: debug db/test.sqlite
	cargo run

.PHONY: debug
debug: target/debug/trails

# Touch needed becaues Cargo won't change the modification time if there's nothing to do.
target/debug/trails: $(rust_source_files) $(html_template_files)
	cargo build
	touch $@

db/test.sqlite: db/build-test-db.sh db/scripts/*.sql db/scripts/01-ISO3166.sql
	sh $< $@

ISO3166/data.json: ISO3166/scrape.py
	python3 $< $@

db/scripts/01-ISO3166.sql db/scripts/01-country.csv db/scripts/01-area.csv: ISO3166/sql.py ISO3166/data.json
	python3 $^ db/scripts/01-ISO3166.sql db/scripts/01-country.csv db/scripts/01-area.csv

ISO3166/crate/src/data.rs: ISO3166/rust.py ISO3166/data.json
	python3 $^ $@